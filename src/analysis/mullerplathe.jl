using Statistics

export mp_fit, mp_slope

"""
    linest(x, y)

Get the parameters of the linear regression `y = a * x + b` and their uncertainties.
Output format: `(slope=a, intercept=b, varslope=σa², varinter=σb²)`.
"""
function linest(x::AbstractVector, y::AbstractVector)
    n = length(x)
    xave = mean(x)
    yave = mean(y)
    Δ = var(x, corrected=false, mean=xave)
    Δ₁ = cov(x, y, corrected=false)
    a = Δ₁ / Δ
    b = yave - a * xave
    rss = mapreduce((x, y)->(y - muladd(x, a, b))^2, +, x, y)
    σa² = rss / ((n - 2) * n * Δ)
    σb² = rss * (1 + mean(x->x^2, x) / Δ) / n
    return (slope=a, intercept=b, varslope=σa², varinter=σb²)
end

"""
    mp_fit(xdata, vdata, inds)

Return the parameters of linear regression `v = a * x + b` and their uncertainties
using indices `inds` for the regression.
"""
function mp_fit(xdata, vdata, inds::AbstractRange=1:length(vdata)÷2)
    return @views linest(xdata[inds], vdata[inds])
end

"""
    mp_fit(xdata, vdata, indsleft, indsright)

Return the average of two slopes in the Müller-Plathe velocity profile.
Index ranges `indsleft` and `indsright` are used for the regression.
"""
function mp_slope(xdata,
                  vdata,
                  indsl::AbstractRange=1:length(vdata)÷2,
                  indsr::AbstractRange=length(vdata)÷2:length(vdata))
    al, _, varl = mp_fit(xdata, vdata, indsl)
    ar, _, varr = mp_fit(xdata, vdata, indsr)
    return middle(abs(al), abs(ar)), varl + varr
end
