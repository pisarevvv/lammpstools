export colnames, print_aveprofiles, print_profiles, collect_profile

"""
Data structure to keep LAMMPS ave/chunk data.
Field names are ave/chunk column names.
Data are averages and variances of values.
"""
struct ProfileBuffer{T}
    description::String
    nrecords::Vector{Int}
    average::T
    variance::T
    function ProfileBuffer(descr, names)
        sanesymbol(s) = Symbol(replace(s, r"[[:^word:]]"=>"_"))
        namedarr(name) = sanesymbol(name) => Float64[]
        ave = (; map(namedarr, names)...)
        var = (; map(namedarr, names)...)
        return new{typeof(ave)}(descr, Int[], ave, var)
    end
end

@inline function Base.getproperty(buf::ProfileBuffer{T}, p::Symbol) where {T}
    if p in fieldnames(T)
        return (nrecords=getfield(buf, :nrecords),
                average=getfield(getfield(buf, :average), p),
                variance=getfield(getfield(buf, :variance), p))
    else
        return getfield(buf, p)
    end
end

function Base.propertynames(::ProfileBuffer{T}) where {T}
    return (fieldnames(ProfileBuffer{T})..., fieldnames(T)...)
end

"""
    buf_chunks(buf)

Return the number of chunks in the buffer.
"""
function buf_chunks(ave_buf::ProfileBuffer)
    return length(ave_buf.nrecords)
end

colnames(::ProfileBuffer{T}) where {T} = (:nrecords, fieldnames(T)...)

function Base.show(io::IO, buf::ProfileBuffer)
    print(io, "ProfileBuffer\n", buf.description, " (", buf_chunks(buf), " chunks)\n")
    join(io, colnames(buf), " ")
end

"""
    print_aveprofiles([io=stdout], buf::ProfileBuffer)

Print the table of average values in `buf` into the specified `io` stream.
"""
function print_aveprofiles(io::IO, buf::ProfileBuffer)
    print(io, buf.description, '\n')
    print(io, "Nchunk ")
    join(io, colnames(buf), " ")
    for i in 1:buf_chunks(buf)
        print(io, '\n', i, ' ', buf.nrecords[i], ' ')
        join(io, (arr[i] for arr in buf.average), " ")
    end
    println(io)
end

print_aveprofiles(buf::ProfileBuffer) = print_aveprofiles(stdout, buf)

"""
    print_aveprofiles([io=stdout], buf::ProfileBuffer)

Print the table of average values and variances in `buf` into the specified `io` stream.
"""
function print_profiles(io::IO, buf::ProfileBuffer)
    print(io, buf.description, '\n')
    print(io, "Nchunk ")
    join(io, colnames(buf), " ")
    for i in 1:buf_chunks(buf)
        print(io, '\n', i, ' ', buf.nrecords[i], ' ')
        for (ave, var) in zip(buf.average, buf.variance)
            join(io, (ave[i], '±', sqrt(var[i] / buf.nrecords[i])))
            print(io, ' ')
        end
    end
    println(io)
end

print_profiles(buf::ProfileBuffer) = print_profiles(stdout, buf)

"""
    collect_profile(filename [; nskip, steprange])

Get the averaged profile from a file containing otuput of LAMMPS `fix ave/chunk` command.
Optionally, skip the first `nskip` records and/or specify the range `steprange` from which
to take profiles for averaging.
"""
function collect_profile(io::IO; nskip::Integer=0, steprange::AbstractRange=0:typemax(Int))
    descr = readline(io)
    readline(io)
    colnames = split(readline(io))[3:end]
    buf = ProfileBuffer(descr, colnames)
    skipped = 0
    while !eof(io) && skipped < nskip
        skipped += 1
        tstep, nchunks, = read_profile_header(io)
        skip_profile(io, nchunks)
    end
    _read_rest_profile!(buf, io, steprange)
    return buf
end

collect_profile(name::AbstractString; kwargs...) = open(name) do io collect_profile(io; kwargs...) end

function skip_profile(io, nchunks)
    for _ in 1:nchunks
        eof(io) && return
        readline(io)
    end
    return
end

function _read_rest_profile!(buf::ProfileBuffer, io::IO, steprange::AbstractRange)
    corr_sum = map(_->Float64[], buf.average)
    corr_var = map(_->Float64[], buf.variance)
    strbufs = map(_->SubString{String}[], buf.average)
    valbufs = map(_->Float64[], buf.average)
    while !eof(io)
        read_profile!(buf, io, steprange, corr_sum, corr_var, strbufs, valbufs)
    end
    for arr in buf.average
        arr ./= buf.nrecords
    end
    for arr in buf.variance
        arr ./= buf.nrecords .- 1
    end
end

"""
    read_profile_header(io)

Read the header for LAMMPS `fix ave/chunk` profile. Its format:
```
Timestep NumberOfChunks TotalCount
```
"""
function read_profile_header(io::IO)
    nums = parse.(Int, readline(io) |> split)
    return (nums[1], nums[2], nums[3])
end

function read_profile!(ave_buf::ProfileBuffer,
                       io::IO,
                       steprange::AbstractRange,
                       kahan_corr_sum,
                       kahan_corr_var,
                       strbufs,
                       valbufs
                      )
    tstep, nchunks, = read_profile_header(io)
    if !(tstep in steprange)
        skip_profile(io, nchunks)
        return ave_buf
    end
    init_length = buf_chunks(ave_buf)
    if nchunks > init_length
        append!(ave_buf.nrecords, Iterators.repeated(0, nchunks - init_length))
        for arr in (ave_buf.average...,
                    ave_buf.variance...,
                    kahan_corr_sum...,
                    kahan_corr_var...)
            append!(arr, Iterators.repeated(0.0, nchunks - init_length))
        end
    end
    foreach(empty!, strbufs)
    @inbounds for i in 1:nchunks
        eof(io) && break
        peek(io) == UInt8(' ') || break
        profile_str = @view split(readline(io))[2:end]
        if length(profile_str) == length(strbufs)
            # string has the correct number of values, i.e. not malformed
            foreach(push!, strbufs, profile_str)
            ave_buf.nrecords[i] += 1
        end
    end
    foreach(valbufs, strbufs) do vbuf, sbuf; resize!(vbuf, length(sbuf)) end
    for (strbuf, valbuf) in zip(strbufs, valbufs)
        map!(valbuf, strbuf) do s parse(Float64, s) end
    end
    # Kahan summation
    for (arr_sum, corr_sum, arr_var, corr_var, profile_vals) in zip(ave_buf.average,
                                                                    kahan_corr_sum,
                                                                    ave_buf.variance,
                                                                    kahan_corr_var,
                                                                    valbufs)
        for i in eachindex(profile_vals)
            val = profile_vals[i]
            nrec = ave_buf.nrecords[i]
            corr_sum[i] += val
            tmpsum = arr_sum[i] + corr_sum[i]
            corr_sum[i] -= tmpsum - arr_sum[i]
            arr_sum[i] = tmpsum
            var_to_add = nrec == 1 ? 0.0 : (nrec * val - arr_sum[i])^2 / (nrec * (nrec - 1))
            corr_var[i] += var_to_add
            tmpsum = arr_var[i] + corr_var[i]
            corr_var[i] -= tmpsum - arr_var[i]
            arr_var[i] = tmpsum
        end
    end
    return ave_buf
end
